package com.springcore.auth.service;

import com.springcore.auth.dto.user.UserResponse;
import com.springcore.auth.entity.oauth.User;

import java.security.Principal;

public interface IUserService {
    UserResponse getDetailProfile(Principal principal);

    User getUserIdToken(Principal principal);

}
