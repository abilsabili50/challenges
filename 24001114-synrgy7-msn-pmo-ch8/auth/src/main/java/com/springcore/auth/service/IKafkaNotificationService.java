package com.springcore.auth.service;

public interface IKafkaNotificationService {
    void sendNotification(String message);
}
