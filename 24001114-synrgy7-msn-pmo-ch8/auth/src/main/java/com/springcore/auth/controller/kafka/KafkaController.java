package com.springcore.auth.controller.kafka;//package com.example.batch7.ch4.controller.kafka;


import com.springcore.auth.service.IKafkaNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user-login/kafka/")
public class KafkaController {

    @Autowired
    private IKafkaNotificationService producer;

    @PostMapping("/publish")
    public void writeMessageToTopic(@RequestParam("message") String message){
        this.producer.sendNotification(message);
        //how to set up dynamic cluster kafka in spring boot ?

    }

}
