package com.springcore.auth.service;

public interface IFirebaseMessagingService {
    void sendFirebaseNotificationToAll(String promoMessage);
}