package com.springcore.auth.service.impl;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.springcore.auth.service.IFirebaseMessagingService;
import org.springframework.stereotype.Service;

@Service
public class FirebaseMessagingService implements IFirebaseMessagingService {
    @Override
    public void sendFirebaseNotificationToAll(String promoMessage) {
        Message message = Message.builder()
                .setTopic("allDevices")
                .setNotification(Notification.builder()
                        .setTitle("Promo Notification")
                        .setBody(promoMessage)
                        .build())
                .build();

        try {
            FirebaseMessaging.getInstance().send(message);
            System.out.println("Sent message to Firebase: " + promoMessage);
        }catch (FirebaseMessagingException err){
            System.err.println("Error sending message to Firebase: " + err.getMessage());
        }
    }
}
