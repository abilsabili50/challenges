package com.springcore.auth.service.impl;

import com.springcore.auth.service.ISchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

@Service
public class SchedulerService implements ISchedulerService {
    @Autowired
    private TaskScheduler taskScheduler;

    private ScheduledFuture<?> scheduledFuture;

    @Override
    public void scheduleTask(String cronExpression) {
        scheduledFuture = taskScheduler.schedule(new RunnableTask(), new CronTrigger(cronExpression));
    }

    @Override
    public void stopScheduleTask() {
        if (scheduledFuture != null){
            scheduledFuture.cancel(false);
        }
    }

    private static class RunnableTask implements Runnable {
        @Override
        public void run() {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd-MM-yyyy HH:mm:ss.SSS");

                String strDate = dateFormat.format(new Date());

                System.out.println(
                        "Fixed Delay Scheduler: Task running at - "
                                + strDate);
        }
    }
}
