package com.springcore.auth.controller.cronjob;

import com.springcore.auth.dto.Response;
import com.springcore.auth.service.ISchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-login/cron")
public class CronJobController {
    @Autowired
    private ISchedulerService schedulerService;

    @GetMapping({"/start", "/start/"})
    public Response<String> createSchedule(@RequestParam String cronExpression){
        schedulerService.scheduleTask(cronExpression);
        return Response.<String>builder()
                .status("OK")
                .message("Scheduled task created with cron: " + cronExpression)
                .build();
    }

    @GetMapping({"/stop", "/stop/"})
    public Response<String> stopSchedule(){
        schedulerService.stopScheduleTask();
        return Response.<String>builder()
                .status("OK")
                .message("Scheduled task stopped")
                .build();
    }
}
