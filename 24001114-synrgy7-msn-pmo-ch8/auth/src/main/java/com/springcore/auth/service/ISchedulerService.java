package com.springcore.auth.service;

public interface ISchedulerService {
    void scheduleTask(String cronExpression);
    void stopScheduleTask();
}
