package com.springcore.auth.service.impl;

import com.springcore.auth.service.IKafkaNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class KafkaNotificationService implements IKafkaNotificationService {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    @Qualifier(value = "taskExecutor")
    private TaskExecutor taskExecutor;

    private final String TOPIC = "promo_notification";
    @Override
    public void sendNotification(String message) {
        kafkaTemplate.send(TOPIC, message);
        System.out.println("Sent message to kafka: " + message);
    }
//
//    @Override
//    public void createSchedule(final String cronFormat){
//        taskExecutor.execute(new Runnable() {
//            @Override
//            @Scheduled(cron = cronFormat)
//            public void run() {
//                SimpleDateFormat dateFormat = new SimpleDateFormat(
//                        "dd-MM-yyyy HH:mm:ss.SSS");
//
//                String strDate = dateFormat.format(new Date());
//
//                System.out.println(
//                        "Fixed Delay Scheduler: Task running at - "
//                                + strDate);
//            }
//        });
//    }
}
