package com.springcore.auth.service.impl;

import com.springcore.auth.dto.user.UserResponse;
import com.springcore.auth.entity.oauth.User;
import com.springcore.auth.repository.oauth.IUserRepository;
import com.springcore.auth.service.IUserService;
import com.springcore.auth.service.oauth.Oauth2UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class UserService implements IUserService {

    @Autowired
    private Oauth2UserDetailsService userDetailsService;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserResponse getDetailProfile(Principal principal) {
        User user = getUserIdToken(principal);
        return UserResponse.builder()
                .id(user.getId())
                .fullname(user.getFullname())
                .username(user.getUsername())
                .build();
    }

    @Override
    public User getUserIdToken(Principal principal) {
        UserDetails userDetails;
        String username = principal.getName();
        if (username.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        userDetails = userDetailsService.loadUserByUsername(username);

        return userRepository.findOneByUsername(userDetails.getUsername()).orElseThrow(()->new UsernameNotFoundException("User name not found"));
    }
}
