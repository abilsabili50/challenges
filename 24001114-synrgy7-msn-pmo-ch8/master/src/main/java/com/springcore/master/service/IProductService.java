package com.springcore.master.service;

import com.springcore.master.dto.product.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IProductService {
    ProductResponse create(CreateProductRequest request);

    ProductResponse getById(String productId);

    Page<ProductResponse> listProducts(Pageable pageable);

    ProductResponse update(UpdateProductRequest request);

    void delete(String productId);
}
