package com.pmo.springcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PemesananMakananOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PemesananMakananOnlineApplication.class, args);
	}

}
