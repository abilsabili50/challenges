package com.pmo.springcore.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response<T> {
    private String status; // "OK", "ERROR"

    private String message; // "user created successfully"

    private T data;

    private String errors; // error message from exception
}
