package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.user.LoginRequest;
import com.pmo.springcore.dto.user.TokenResponse;
import com.pmo.springcore.service.IAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/auth")
@Slf4j
public class AuthController {

    private final IAuthService authService;


    @Autowired
    public AuthController(IAuthService authService){
        this.authService = authService;
    }

    @PostMapping(
            path = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<TokenResponse> login(@RequestBody LoginRequest request){
        log.info("controller : new login request");
        TokenResponse response = authService.login(request);
        return Response.<TokenResponse>builder().status("OK").message("login success").data(response).build();
    }
}
