package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.product.CreateProductRequest;
import com.pmo.springcore.dto.product.ProductResponse;
import com.pmo.springcore.dto.product.UpdateProductRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/api/v1/products")
@Slf4j
public class ProductController {

    private final IProductService productService;

    @Autowired
    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @PostMapping(
            path = "/",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> create(User user, @RequestBody CreateProductRequest request){
        log.info("controller : create new product by user-id : " + user.getId());
        ProductResponse productResponse = productService.create(user, request);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product created successfully")
                .data(productResponse)
                .build();
    }

    @GetMapping(
            path = "/{productId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> getById(@PathVariable String productId){
        log.info("controller : get product by id");
        ProductResponse productResponse = productService.getById(productId);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product found")
                .data(productResponse)
                .build();
    }

    @GetMapping(
            path = "/list",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<ProductResponse>> listProducts(Pageable pageable){
        log.info("controller : get product list");
        Page<ProductResponse> productResponseList = productService.listProducts(pageable);
        return Response.<Page<ProductResponse>>builder()
                .status("OK")
                .message("here's products list")
                .data(productResponseList)
                .build();
    }

    @PatchMapping(
            path = "/{productId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> update(User user, @RequestBody UpdateProductRequest request, @PathVariable String productId){
        log.info("controller : update product by user-id : " + user.getId());
        ProductResponse productResponse = productService.update(user, request, productId);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product updated successfully")
                .data(productResponse)
                .build();
    }

    @DeleteMapping(
            path = "/{productId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(User user, @PathVariable String productId){
        log.info("controller : delete product by user-id : " + user.getId());
        productService.delete(user, productId);
        return Response.builder()
                .status("OK")
                .message("product deleted successfully")
                .build();
    }
}
