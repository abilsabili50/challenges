package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
@Slf4j
public class ErrorController {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Response<String>> constraintViolationException(ConstraintViolationException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Response.<String>builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Response<Object>> responseStatusException(ResponseStatusException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(exception.getStatusCode())
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler
    public ResponseEntity<Response<Object>> runtimeException(RuntimeException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Response.builder().errors(exception.getMessage()).status("ERROR").build());
    }

    @ExceptionHandler
    public ResponseEntity<Response<Object>> mediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exception){
        log.error("error : " + exception.getMessage());
        return ResponseEntity.status(exception.getStatusCode())
                .body(Response.builder().errors("media type not supported").status("ERROR").build());
    }

}
