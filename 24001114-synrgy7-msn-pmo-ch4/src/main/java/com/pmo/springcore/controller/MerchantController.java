package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.merchant.CreateMerchantRequest;
import com.pmo.springcore.dto.merchant.MerchantResponse;
import com.pmo.springcore.dto.merchant.UpdateMerchantRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IMerchantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/merchants")
@Slf4j
public class MerchantController {

    private final IMerchantService merchantService;

    @Autowired
    public MerchantController(IMerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @PostMapping(
            path = "/",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> create(User user, @RequestBody CreateMerchantRequest request) {
        log.info("controller : create merchant by user-id : " + user.getId());
        MerchantResponse merchantResponse = merchantService.create(user, request);
        return Response.<MerchantResponse>builder().status("OK").message("merchant created successfully").data(merchantResponse).build();
    }

    @GetMapping(
            path = "/{merchantId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> getById(@PathVariable String merchantId) {
        log.info("controller : get merchant by id");
        MerchantResponse merchantResponse = merchantService.getById(merchantId);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant found")
                .data(merchantResponse)
                .build();
    }

    @GetMapping(
            path = "/",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<MerchantResponse>> listMerchants(Pageable pageable, @RequestParam(name = "is_open", defaultValue = "true") Boolean isOpen){
        log.info("controller : get merchant lists");
        Page<MerchantResponse> merchantResponsesList = merchantService.listOpenMerchant(isOpen, pageable);
        return Response.<Page<MerchantResponse>>builder()
                .status("OK")
                .message("here's all merchants")
                .data(merchantResponsesList)
                .build();
    }

    @PatchMapping(
            path = "/{merchantId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> update(User user, @RequestBody UpdateMerchantRequest req, @PathVariable String merchantId) {
        log.info("controller : update merchant by user-id : " + user.getId());
        req.setId(merchantId);
        MerchantResponse updateResponse = merchantService.update(user, req);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant updated successfully")
                .data(updateResponse)
                .build();

    }

    @DeleteMapping(
            path = "/{merchantId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(User user, @PathVariable String merchantId){
        log.info("controller : delete merchant by user-id : " + user.getId());
        merchantService.delete(user, merchantId);
        return Response.builder().status("OK").message("merchant deleted successfully").build();
    }
}
