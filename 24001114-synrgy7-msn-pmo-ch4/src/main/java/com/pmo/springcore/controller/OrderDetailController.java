package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.detail.CreateOrderDetailRequest;
import com.pmo.springcore.dto.order.detail.OrderDetailResponse;
import com.pmo.springcore.dto.order.detail.UpdateOrderDetailRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IOrderDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/api/v1/orders")
@Slf4j
public class OrderDetailController {
    private final IOrderDetailService orderDetailService;

    @Autowired
    public OrderDetailController(IOrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    @PostMapping(
            path = "/details",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> create(User user, @RequestBody CreateOrderDetailRequest request){
        log.info("controller : create order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.create(user, request);

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("order detail created successfully")
                .data(orderDetailResponse)
                .build();
    }

    @GetMapping(
            path = "/details/{orderDetailId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> getById(User user, @PathVariable String orderDetailId){
        log.info("controller : get order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.getById(user, orderDetailId);

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("detail order found")
                .data(orderDetailResponse)
                .build();
    }

    @GetMapping(
            path = "/{orderId}/details",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<List<OrderDetailResponse>> listByOrderId(User user, @PathVariable String orderId){
        log.info("controller : get list order details by user-id : " + user.getId());
        List<OrderDetailResponse> orderDetailResponsesList = orderDetailService.listByOrderId(user, orderId);

        return Response.<List<OrderDetailResponse>>builder()
                .status("OK")
                .message("here's the order details")
                .data(orderDetailResponsesList)
                .build();
    }

    @PatchMapping(
            path = "/details/{orderDetailId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> update(User user, @PathVariable String orderDetailId, @RequestBody UpdateOrderDetailRequest request){
        log.info("controller : update order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.update(user, orderDetailId, request);

        String message = (Objects.isNull(orderDetailResponse))
                ? "detail order has been deleted"
                : "detail order updated successfully";

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message(message)
                .data(orderDetailResponse)
                .build();
    }

    @DeleteMapping(
            path = "/details/{orderDetailId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(User user, @PathVariable String orderDetailId){
        log.info("controller : update order detail by user-id : " + user.getId());
        orderDetailService.delete(user, orderDetailId);

        return Response.builder()
                .status("OK")
                .message("detail order deleted successfully")
                .build();
    }
}
