package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.user.RegisterRequest;
import com.pmo.springcore.dto.user.UpdateUserRequest;
import com.pmo.springcore.dto.user.UserResponse;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IUserRepository;
import com.pmo.springcore.service.IUserService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;

@Service
public class UserService implements IUserService {

    private final IUserRepository userRepository;

    private final ValidationService validator;

    @Autowired
    public UserService(IUserRepository userRepository, ValidationService validator){
        this.userRepository = userRepository;
        this.validator = validator;
    }

    @Transactional
    @Override
    public void register(RegisterRequest request){
        validator.validate(request);

        if(userRepository.existsByEmailAddress(request.getEmail())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "email already exists");
        }

        User user = new User();
        user.setEmailAddress(request.getEmail());
        user.setUsername(request.getUsername());
        user.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        user.setRoleId((request.getRoleId() != null) ? request.getRoleId() : 1);

        userRepository.save(user);
    }

    @Override
    public UserResponse get(User user){
        return UserResponse.builder()
                .username(user.getUsername())
                .email(user.getEmailAddress())
                .id(user.getId().toString())
                .build();
    }

    @Override
    @Transactional
    public UserResponse update(User user, UpdateUserRequest request){
        validator.validate(request);

        if(Objects.nonNull(request.getUsername())){
            user.setUsername(request.getUsername());
        }

        if(Objects.nonNull(request.getEmail())){
            user.setEmailAddress(request.getEmail());
        }

        if(Objects.nonNull(request.getPassword())){
            user.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        }

        userRepository.save(user);

        return UserResponse.builder().id(user.getId().toString()).email(user.getEmailAddress()).username(request.getUsername()).build();
    }

    @Override
    @Transactional
    public void delete(User user){
        userRepository.delete(user);
    }
}
