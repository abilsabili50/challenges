package com.pmo.springcore.service;

import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IOrderService {
    OrderResponse create(User user, CreateOrderRequest request);

    OrderResponse getById(User user, String orderId);

    Page<OrderResponse> listOrder(User user, Boolean completed, Pageable pageable);

    OrderResponse update(User user, UpdateOrderRequest request);

    void delete(User user, String orderId);
}
