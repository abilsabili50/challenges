package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.order.detail.CreateOrderDetailRequest;
import com.pmo.springcore.dto.order.detail.OrderDetailResponse;
import com.pmo.springcore.dto.order.detail.UpdateOrderDetailRequest;
import com.pmo.springcore.entity.Order;
import com.pmo.springcore.entity.OrderDetail;
import com.pmo.springcore.entity.Product;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IOrderDetailRepository;
import com.pmo.springcore.repository.IOrderRepository;
import com.pmo.springcore.repository.IProductRepository;
import com.pmo.springcore.service.IOrderDetailService;
import com.pmo.springcore.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@Service
public class OrderDetailService implements IOrderDetailService {

    private final IOrderDetailRepository orderDetailRepository;

    private final IOrderRepository orderRepository;

    private final IProductRepository productRepository;

    private final ValidationService validator;

    @Autowired
    public OrderDetailService(
            IOrderDetailRepository orderDetailRepository,
            IOrderRepository orderRepository,
            IProductRepository productRepository,
            ValidationService validator
    ) {
        this.orderDetailRepository = orderDetailRepository;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.validator = validator;
    }

    @Override
    @Transactional
    public OrderDetailResponse create(User user, CreateOrderDetailRequest request) {
        validator.validate(request);

        UUID orderUUID = Util.convertStringIntoUUID(request.getOrderId());
        UUID productUUID = Util.convertStringIntoUUID(request.getProductId());

        Order order = orderRepository.findByIdAndUserId(orderUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order not found"));
        Product product = productRepository.findById(productUUID).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "product not found"));

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setQuantity(request.getQuantity());
        orderDetail.setTotalPrice(request.getQuantity() * product.getPrice());
        orderDetail.setOrder(order);
        orderDetail.setProduct(product);
        orderDetailRepository.save(orderDetail);

        return OrderDetailResponse.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                .orderId(orderDetail.getOrder().getId())
                .productId(orderDetail.getProduct().getId())
                .build();
    }

    @Override
    public OrderDetailResponse getById(User user, String orderDetailId) {
        UUID orderDetailUUID = Util.convertStringIntoUUID(orderDetailId);

        OrderDetail orderDetail = orderDetailRepository.findById(orderDetailUUID).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        // check the order based on the order detail id provided by user
        orderRepository.findByIdAndUserId(orderDetail.getOrder().getId(), user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        return OrderDetailResponse.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                .orderId(orderDetail.getOrder().getId())
                .productId(orderDetail.getProduct().getId())
                .build();
    }

    @Override
    public List<OrderDetailResponse> listByOrderId(User user, String orderId) {
        UUID orderUUID = Util.convertStringIntoUUID(orderId);

        orderRepository.findByIdAndUserId(orderUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order detail not found"));

        return orderDetailRepository.findAllByOrderId(orderUUID).stream().map(orderDetail -> OrderDetailResponse.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                .orderId(orderDetail.getOrder().getId())
                .productId(orderDetail.getProduct().getId())
                .build()).toList();
    }

    @Override
    @Transactional
    public OrderDetailResponse update(User user, String orderDetailId, UpdateOrderDetailRequest request) {
        validator.validate(request);

        UUID orderDetailUUID = Util.convertStringIntoUUID(orderDetailId);

        OrderDetail orderDetail = orderDetailRepository.findById(orderDetailUUID).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        // check the relation of user and order
        orderRepository.findByIdAndUserId(orderDetail.getOrder().getId(), user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        // get product price to update total price
        Product product = productRepository.findById(orderDetail.getProduct().getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order detail not found"));
        orderDetail.setTotalPrice(request.getQuantity() * product.getPrice());
        orderDetail.setQuantity(request.getQuantity());

        // check if request quantity <= 0, so the order detail should be deleted
        if(request.getQuantity() <= 0){
            orderDetailRepository.deleteById(orderDetail.getId());
        }else {
            orderDetailRepository.save(orderDetail);
        }

        // also this should return null if order detail has been deleted
        return (request.getQuantity() <= 0)
                ? null
                : OrderDetailResponse.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                .orderId(orderDetail.getOrder().getId())
                .productId(orderDetail.getProduct().getId())
                .build();
    }

    @Override
    @Transactional
    public void delete(User user, String orderDetailId) {
        UUID orderDetailUUID = Util.convertStringIntoUUID(orderDetailId);

        OrderDetail orderDetail = orderDetailRepository.findById(orderDetailUUID).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        orderRepository.findByIdAndUserId(orderDetail.getOrder().getId(), user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "detail order not found"));

        orderDetailRepository.deleteById(orderDetail.getId());

    }
}
