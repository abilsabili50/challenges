package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.merchant.CreateMerchantRequest;
import com.pmo.springcore.dto.merchant.MerchantResponse;
import com.pmo.springcore.dto.merchant.UpdateMerchantRequest;
import com.pmo.springcore.entity.Merchant;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IMerchantRepository;
import com.pmo.springcore.service.IMerchantService;
import com.pmo.springcore.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@Service
public class MerchantService implements IMerchantService {

    private final IMerchantRepository merchantRepository;
    private final ValidationService validator;

    @Autowired
    public MerchantService(IMerchantRepository merchantRepository, ValidationService validator) {
        this.merchantRepository = merchantRepository;
        this.validator = validator;
    }

    @Override
    @Transactional
    public MerchantResponse create(User user, CreateMerchantRequest request) {
        validator.validate(request);

        validator.validateBuyerRole(user.getRoleId());

        if (merchantRepository.existsByMerchantNameAndUserId(request.getMerchantName(), user.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "merchant name already exists");
        }

        Merchant merchant = new Merchant();
        merchant.setMerchantName(request.getMerchantName());
        merchant.setMerchantLocation(request.getMerchantLocation());
        merchant.setOpen(request.getIsOpen());
        merchant.setUser(user);

        merchantRepository.save(merchant);

        return MerchantResponse.builder()
                .id(merchant.getId())
                .merchantName(merchant.getMerchantName())
                .merchantLocation(merchant.getMerchantLocation())
                .isOpen(merchant.isOpen())
                .build();
    }

    @Override
    public MerchantResponse getById(String merchantId){
        UUID castedMerchantId = Util.convertStringIntoUUID(merchantId);

        Merchant merchant = merchantRepository.findById(castedMerchantId).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "merchant not found"));

        return MerchantResponse.builder()
                .id(merchant.getId())
                .merchantName(merchant.getMerchantName())
                .merchantLocation(merchant.getMerchantLocation())
                .isOpen(merchant.isOpen())
                .build();
    }

    @Override
    public Page<MerchantResponse> listOpenMerchant(Boolean isOpen, Pageable pageable) {
        Page<Merchant> pageMerchants = merchantRepository.findAllByIsOpen(isOpen, pageable);

        List<MerchantResponse> merchantResponseList = pageMerchants.getContent().stream().map(merchant -> MerchantResponse.builder()
                .id(merchant.getId())
                .merchantName(merchant.getMerchantName())
                .merchantLocation(merchant.getMerchantLocation())
                .isOpen(merchant.isOpen())
                .build()).toList();

        return new PageImpl<>(merchantResponseList, pageMerchants.getPageable(), pageMerchants.getTotalElements());
    }

    @Override
    @Transactional
    public MerchantResponse update(User user, UpdateMerchantRequest req) {
        validator.validate(req);

        validator.validateBuyerRole(user.getRoleId());

        UUID merchantId = Util.convertStringIntoUUID(req.getId());

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "merchant not found"));

        merchant.setOpen(req.getIsOpen());

        merchantRepository.save(merchant);

        return MerchantResponse.builder()
                .id(merchant.getId())
                .merchantName(merchant.getMerchantName())
                .merchantLocation(merchant.getMerchantLocation())
                .build();
    }

    @Override
    @Transactional
    public void delete(User user, String merchantId){
        validator.validateBuyerRole(user.getRoleId());

        UUID merchantUUID = Util.convertStringIntoUUID(merchantId);

        merchantRepository.findByIdAndUserId(merchantUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "merchant not found"));

        merchantRepository.deleteById(merchantUUID);
    }

    // yang belum tau caranya yaitu bagaimana jika kita ingin menghapus merchant
    // yang sudah memiliki product? apakah product harus dihapus manual atau otomatis
    // product akan terhapus secara langsung
}
