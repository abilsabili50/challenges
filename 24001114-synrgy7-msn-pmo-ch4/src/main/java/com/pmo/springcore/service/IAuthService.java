package com.pmo.springcore.service;

import com.pmo.springcore.dto.user.LoginRequest;
import com.pmo.springcore.dto.user.TokenResponse;

public interface IAuthService {

    TokenResponse login(LoginRequest request);

}
