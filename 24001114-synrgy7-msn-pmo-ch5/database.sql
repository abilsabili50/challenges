create table users
(
    id               uuid not null,
    username         varchar(255),
    email_address    varchar(255),
    password         varchar(255),
    token            varchar(255),
    token_expired_at bigint,
    constraint pk_users primary key (id)
);

alter table users add constraint unique_email unique (email_address);

alter table users add role_id int;

alter table users add constraint fk_users_role foreign key (role_id) references role(id);

create table role(
    id  int not null,
    role varchar(100) not null,
    constraint pk_role primary key (id)
);

insert into role (id, role)values(1, 'buyer'), (2, 'owner');

select * from role;

select * from users;

delete from users where email_address='test@test.test';

create table merchants
(
    id                uuid not null,
    merchant_name     varchar(255),
    merchant_location varchar(500),
    is_open           bool,
    constraint pk_merchant primary key (id)
);

alter table merchants add user_id uuid;

alter table merchants add constraint fk_merchants_users foreign key (user_id) references users(id);

alter table merchants alter column user_id set not null;

create table orders
(
    id                  uuid    not null,
    order_time          timestamp not null default current_timestamp,
    destination_address varchar(500),
    user_id             uuid       not null,
    completed           bool,
    constraint pk_orders primary key (id),
    constraint fk_users foreign key (user_id) references users (id)
);

create table products
(
    id           uuid not null,
    product_name varchar(255),
    price        int    not null,
    merchant_id  uuid    not null,
    constraint pk_product primary key (id),
    constraint fk_merchant foreign key (merchant_id) references merchants (id)
);

create table order_detail
(
    id          uuid not null,
    order_id    uuid    not null,
    product_id  uuid    not null,
    quantity    int    not null,
    total_price int    not null,
    constraint pk_order_detail primary key (id),
    constraint fk_order foreign key (order_id) references orders (id),
    constraint fk_product foreign key (product_id) references products (id)
);