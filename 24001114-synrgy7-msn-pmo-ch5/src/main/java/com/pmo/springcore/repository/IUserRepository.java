package com.pmo.springcore.repository;

import com.pmo.springcore.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IUserRepository extends JpaRepository<User, UUID> {
    Boolean existsByEmailAddress(String email);
    Optional<User> findFirstByEmailAddress(String email);
    Optional<User> findFirstByToken(String token);
}
