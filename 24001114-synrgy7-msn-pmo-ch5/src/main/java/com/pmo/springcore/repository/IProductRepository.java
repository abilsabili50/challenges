package com.pmo.springcore.repository;

import com.pmo.springcore.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface IProductRepository extends JpaRepository<Product, UUID> {
    Boolean existsByProductNameAndMerchantId(String productName, UUID merchantId);
    Page<Product> findAll(Pageable pageable);
}
