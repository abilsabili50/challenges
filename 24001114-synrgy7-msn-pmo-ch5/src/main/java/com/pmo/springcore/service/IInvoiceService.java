package com.pmo.springcore.service;

import com.pmo.springcore.entity.User;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

public interface IInvoiceService {
    byte[] generateInvoice(User user) throws JRException;
}
