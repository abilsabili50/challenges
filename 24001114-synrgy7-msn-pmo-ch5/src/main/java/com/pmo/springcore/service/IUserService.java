package com.pmo.springcore.service;

import com.pmo.springcore.dto.user.*;
import com.pmo.springcore.entity.User;

public interface IUserService {
    void register(RegisterRequest request);

    UserResponse get(User user);

    UserResponse update(User user, UpdateUserRequest request);

    void delete(User user);
}
