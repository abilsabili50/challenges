package com.pmo.springcore.service.impl;

import com.pmo.springcore.entity.*;
import com.pmo.springcore.entity.custom.Invoice;
import com.pmo.springcore.entity.custom.InvoiceRow;
import com.pmo.springcore.repository.*;
import com.pmo.springcore.service.IInvoiceService;
import com.pmo.springcore.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Slf4j
public class InvoiceService implements IInvoiceService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IOrderRepository orderRepository;

    @Autowired
    private IOrderDetailRepository orderDetailRepository;

    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IMerchantRepository merchantRepository;

    @Override
    public byte[] generateInvoice(User user) {
        try {
            Invoice invoiceData = getData(user);
            log.info("service : prepared parameters by user-id : " + user.getId());
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("username", invoiceData.getUsername());
            parameters.put("totalExpense", invoiceData.getTotalExpense().toString());

            String FILENAME = "invoice_no_data_template.jrxml";
            JRBeanCollectionDataSource dataSource = null;

            // checker if data is not empty
            if(!invoiceData.getRows().isEmpty()){
                FILENAME = "invoice_template.jrxml";

                log.info("service : create datasource by user-id : " + user.getId());
                dataSource = new JRBeanCollectionDataSource(invoiceData.getRows());
            }

            // generate invoice
            log.info("service : get jrxml file by user-id : " + user.getId());
            File file = ResourceUtils.getFile("classpath:" + FILENAME);

            log.info("service : compile jrxml file by user-id : " + user.getId());
            JasperReport report = JasperCompileManager.compileReport(file.getAbsolutePath());

            log.info("service : pass parameters and datasource into report by user-id : " + user.getId());
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, (dataSource != null) ? dataSource : new JREmptyDataSource());

            log.info("service : export prepared jasper print into pdf file by user-id : " + user.getId());
            return JasperExportManager.exportReportToPdf(jasperPrint);
        }catch (JRException | FileNotFoundException exception){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    private Invoice getData(User user){
        log.info("service : get data by user-id : " + user.getId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy");
        Invoice invoiceData = new Invoice();
        invoiceData.setUsername(user.getUsername());

        List<Order> orders = orderRepository.findAllByUserId(user.getId());
        List<InvoiceRow> rows = new ArrayList<>();
        long totalExpense = 0L;

        for (Order order : orders){
            List<OrderDetail> orderDetails = orderDetailRepository.findAllByOrderId(order.getId());
            for(OrderDetail orderDetail : orderDetails){
                InvoiceRow row = new InvoiceRow();

                Product product = productRepository.findById(orderDetail.getProduct().getId()).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND, "product not found"));
                Merchant merchant = merchantRepository.findById(product.getMerchant().getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "merchant not found"));

                row.setMerchantName(merchant.getMerchantName());
                row.setProductName(product.getProductName());
                row.setDestination(order.getDestinationAddress());
                row.setOrderTime(order.getOrderTime().format(formatter));
                row.setQuantity(orderDetail.getQuantity());
                row.setTotalPrice(orderDetail.getTotalPrice());


                totalExpense += orderDetail.getQuantity() * orderDetail.getTotalPrice();
                rows.add(row);
            }
        }

        invoiceData.setRows(rows);
        invoiceData.setTotalExpense(totalExpense);

        return invoiceData;
    }
}
