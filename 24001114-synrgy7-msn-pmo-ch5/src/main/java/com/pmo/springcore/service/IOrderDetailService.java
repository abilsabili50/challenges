package com.pmo.springcore.service;

import com.pmo.springcore.dto.order.detail.CreateOrderDetailRequest;
import com.pmo.springcore.dto.order.detail.OrderDetailResponse;
import com.pmo.springcore.dto.order.detail.UpdateOrderDetailRequest;
import com.pmo.springcore.entity.User;

import java.util.List;

public interface IOrderDetailService {
    OrderDetailResponse create(User user, CreateOrderDetailRequest request);
    OrderDetailResponse getById(User user, String orderDetailId);
    List<OrderDetailResponse> listByOrderId(User user, String orderId);
    OrderDetailResponse update(User user, String orderDetailId, UpdateOrderDetailRequest request);
    void delete(User user, String orderDetailId);
}
