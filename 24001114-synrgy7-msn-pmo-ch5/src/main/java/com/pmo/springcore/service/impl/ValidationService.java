package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.user.LoginRequest;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@Service
public class ValidationService {

    private final Validator validator;

    @Autowired
    public ValidationService(Validator validator){
        this.validator = validator;
    }

    public void validate(Object request){
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()){
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    public void validateBuyerRole(Integer roleId){
        if(roleId != 2){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "illegal access");
        }
    }
}
