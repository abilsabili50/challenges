package com.pmo.springcore.entity.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private List<InvoiceRow> rows;
    private String username;
    private Long totalExpense;
}
