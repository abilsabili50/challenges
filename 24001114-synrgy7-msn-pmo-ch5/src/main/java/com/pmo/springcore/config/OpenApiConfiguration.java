package com.pmo.springcore.config;

import io.swagger.v3.oas.models.media.Schema;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Configuration
public class OpenApiConfiguration {

    @Bean
    public OpenApiCustomizer openApiCustomizer(){
        return openApi -> {
            Map<String, Schema> schemas = openApi.getComponents().getSchemas();
            if(schemas != null){
                // list of schemas to exclude
                List<String> schemasToHide = Arrays.asList("Merchant", "Order", "OrderDetail", "Product", "User", "Invoice", "InvoiceRow");

                // remove the schemas
                schemasToHide.forEach(schemas::remove);
            }
        };
    }
}
