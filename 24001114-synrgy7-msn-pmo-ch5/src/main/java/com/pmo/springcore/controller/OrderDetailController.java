package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.detail.CreateOrderDetailRequest;
import com.pmo.springcore.dto.order.detail.OrderDetailResponse;
import com.pmo.springcore.dto.order.detail.UpdateOrderDetailRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IOrderDetailService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/orders")
@Slf4j
public class OrderDetailController {
    private final IOrderDetailService orderDetailService;

    @Autowired
    public OrderDetailController(IOrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    @Operation(
            summary = "Create Order Detail",
            description = "this endpoint is used to create new order detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PostMapping(
            path = {"/details", "/details/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> create(@Parameter(hidden = true) User user, @RequestBody CreateOrderDetailRequest request){
        log.info("controller : create order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.create(user, request);

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("order detail created successfully")
                .data(orderDetailResponse)
                .build();
    }

    @Operation(
            summary = "Get Order Detail By Order Detail Id",
            description = "this endpoint is used to get order detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderDetailId",
                            description = "Order Detail Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> getById(@Parameter(hidden = true) User user, @PathVariable String orderDetailId){
        log.info("controller : get order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.getById(user, orderDetailId);

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("detail order found")
                .data(orderDetailResponse)
                .build();
    }

    @Operation(
            summary = "Get List Order Details",
            description = "this endpoint is used to get list of order details and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderId",
                            description = "Order Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/{orderId}/details", "/{orderId}/details/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<List<OrderDetailResponse>> listByOrderId(@Parameter(hidden = true) User user, @PathVariable String orderId){
        log.info("controller : get list order details by user-id : " + user.getId());
        List<OrderDetailResponse> orderDetailResponsesList = orderDetailService.listByOrderId(user, orderId);

        return Response.<List<OrderDetailResponse>>builder()
                .status("OK")
                .message("here's the order details")
                .data(orderDetailResponsesList)
                .build();
    }

    @Operation(
            summary = "Update Order Detail",
            description = "this endpoint is used to update order detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderDetailId",
                            description = "Order Detail Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PatchMapping(
            path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderDetailResponse> update(@Parameter(hidden = true) User user, @PathVariable String orderDetailId, @RequestBody UpdateOrderDetailRequest request){
        log.info("controller : update order detail by user-id : " + user.getId());
        OrderDetailResponse orderDetailResponse = orderDetailService.update(user, orderDetailId, request);

        String message = (Objects.isNull(orderDetailResponse))
                ? "detail order has been deleted"
                : "detail order updated successfully";

        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message(message)
                .data(orderDetailResponse)
                .build();
    }

    @Operation(
            summary = "Delete Order Detail",
            description = "this endpoint is used to delete order detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderDetailId",
                            description = "Order Detail Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @DeleteMapping(
            path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(@Parameter(hidden = true) User user, @PathVariable String orderDetailId){
        log.info("controller : update order detail by user-id : " + user.getId());
        orderDetailService.delete(user, orderDetailId);

        return Response.builder()
                .status("OK")
                .message("detail order deleted successfully")
                .build();
    }
}
