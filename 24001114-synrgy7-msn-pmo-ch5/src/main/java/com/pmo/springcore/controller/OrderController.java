package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IOrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/orders")
@Slf4j
public class OrderController {

    private final IOrderService orderService;

    @Autowired
    public OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }

    @Operation(
            summary = "Create Order",
            description = "this endpoint is used to create new order and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PostMapping(
            path = {"", "/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> create(@Parameter(hidden = true) User user, @RequestBody CreateOrderRequest request) {
        log.info("controller : create order by user-id : " + user.getId());
        OrderResponse orderResponse = orderService.create(user, request);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order created successfully")
                .data(orderResponse)
                .build();

    }

    @Operation(
            summary = "Get Order By Id",
            description = "this endpoint is used to get order by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderId",
                            description = "Order Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/{orderId}", "/{orderId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> getById(@Parameter(hidden = true) User user, @PathVariable String orderId) {
        log.info("controller : get order by id by user-id : " + user.getId());
        OrderResponse orderResponse = orderService.getById(user, orderId);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order created successfully")
                .data(orderResponse)
                .build();
    }

    @Operation(
            summary = "Get Order List",
            description = "this endpoint is used to get list of orders and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/list", "/list/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<OrderResponse>> listOrder(@Parameter(hidden = true) User user, @RequestParam(name = "completed", required = false) Boolean completed, Pageable pageable) {
        log.info("controller : get list orders by user-id : " + user.getId());
        Page<OrderResponse> orderResponseList = orderService.listOrder(user, (Objects.nonNull(completed)) ? completed : null, pageable);
        return Response.<Page<OrderResponse>>builder()
                .status("OK")
                .message("here's the order")
                .data(orderResponseList)
                .build();
    }

    @Operation(
            summary = "Update Order",
            description = "this endpoint is used to update order by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderId",
                            description = "Order Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PatchMapping(
            path = {"/{orderId}", "/{orderId}/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<OrderResponse> update(@Parameter(hidden = true) User user, @RequestBody UpdateOrderRequest request, @PathVariable String orderId) {
        log.info("controller : update order by user-id : " + user.getId());

        request.setId(orderId);

        OrderResponse orderResponse = orderService.update(user, request);

        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order updated successfully")
                .data(orderResponse)
                .build();
    }

    @Operation(
            summary = "Delete Order",
            description = "this endpoint is used to delete order by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "orderId",
                            description = "Order Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @DeleteMapping(
            path = {"/{orderId}", "/{orderId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(@Parameter(hidden = true) User user, @PathVariable String orderId) {
        log.info("controller : delete order by user-id : " + user.getId());
        orderService.delete(user, orderId);

        return Response.builder()
                .status("OK")
                .message("order deleted successfully")
                .build();
    }
}
