package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.merchant.CreateMerchantRequest;
import com.pmo.springcore.dto.merchant.MerchantResponse;
import com.pmo.springcore.dto.merchant.UpdateMerchantRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IMerchantService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/merchants")
@Slf4j
public class MerchantController {

    private final IMerchantService merchantService;

    @Autowired
    public MerchantController(IMerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @Operation(
            summary = "Create Merchant",
            description = "this endpoint is used to create new merchant and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PostMapping(
            path = {"", "/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> create(@Parameter(hidden = true) User user, @RequestBody CreateMerchantRequest request) {
        log.info("controller : create merchant by user-id : " + user.getId());
        MerchantResponse merchantResponse = merchantService.create(user, request);
        return Response.<MerchantResponse>builder().status("OK").message("merchant created successfully").data(merchantResponse).build();
    }

    @Operation(
            summary = "Get Merchant By Id",
            description = "this endpoint is used to get merchant by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "merchantId",
                            description = "Merchant Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/{merchantId}","/{merchantId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> getById(@PathVariable String merchantId) {
        log.info("controller : get merchant by id");
        MerchantResponse merchantResponse = merchantService.getById(merchantId);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant found")
                .data(merchantResponse)
                .build();
    }

    @Operation(
            summary = "Get Merchant List",
            description = "this endpoint is used to get list of merchants by id"
    )
    @GetMapping(
            path = {"/list", "/list/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<MerchantResponse>> listMerchants(Pageable pageable, @RequestParam(name = "is_open", defaultValue = "true") Boolean isOpen){
        log.info("controller : get merchant lists");
        Page<MerchantResponse> merchantResponsesList = merchantService.listOpenMerchant(isOpen, pageable);
        return Response.<Page<MerchantResponse>>builder()
                .status("OK")
                .message("here's all merchants")
                .data(merchantResponsesList)
                .build();
    }

    @Operation(
            summary = "Update Merchant",
            description = "this endpoint is used to update merchant by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "merchantId",
                            description = "Merchant Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PatchMapping(
            path = {"/{merchantId}", "/{merchantId}/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<MerchantResponse> update(@Parameter(hidden = true) User user, @RequestBody UpdateMerchantRequest req, @PathVariable String merchantId) {
        log.info("controller : update merchant by user-id : " + user.getId());
        req.setId(merchantId);
        MerchantResponse updateResponse = merchantService.update(user, req);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant updated successfully")
                .data(updateResponse)
                .build();

    }

    @Operation(
            summary = "Delete Merchant",
            description = "this endpoint is used to delete merchant by id and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "merchantId",
                            description = "Merchant Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @DeleteMapping(
            path = {"/{merchantId}", "/{merchantId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(@Parameter(hidden = true) User user, @PathVariable String merchantId){
        log.info("controller : delete merchant by user-id : " + user.getId());
        merchantService.delete(user, merchantId);
        return Response.builder().status("OK").message("merchant deleted successfully").build();
    }
}
