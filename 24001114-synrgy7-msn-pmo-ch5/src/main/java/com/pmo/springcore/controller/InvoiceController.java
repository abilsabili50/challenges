package com.pmo.springcore.controller;

import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping(path = "/invoices")
public class InvoiceController {

    @Autowired
    private IInvoiceService invoiceService;

    @Operation(
            summary = "Generate Invoices",
            description = "this endpoint is used to generate invoice and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/generate", "/generate/"},
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity<Resource> generatePdf(@Parameter(hidden = true) User user) throws JRException {
        String outputFileName = "INVOICE-" + user.getUsername() + ".pdf";

        log.info("controller : generate pdf into bytecodes by user-id : " + user.getId());
        byte[] pdfBytes = invoiceService.generateInvoice(user);

        log.info("controller : pass pdf bytecodes into byte array resource by user-id : " + user.getId());
        ByteArrayResource resource = new ByteArrayResource(pdfBytes);

        log.info("controller : set response format by user-id : " + user.getId());
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(resource.contentLength())
                .header(
                        HttpHeaders.CONTENT_DISPOSITION,
                        ContentDisposition
                                .attachment()
                                .filename(outputFileName)
                                .build().toString()
                )
                .body(resource);
    }
}
