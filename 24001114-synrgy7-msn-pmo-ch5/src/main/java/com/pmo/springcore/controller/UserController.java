package com.pmo.springcore.controller;

import com.pmo.springcore.dto.user.*;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IAuthService;
import com.pmo.springcore.service.IUserService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/users")
@Slf4j
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
    }

    @Operation(
            summary = "Register New User",
            description = "this endpoint is used to create new user"
    )
    @PostMapping(
            path = {"/register", "/register/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> register(@RequestBody RegisterRequest request) {
        log.info("controller : new register request");
        userService.register(request);
        return Response.builder().status("OK").message("user created successfully").build();
    }

    @Operation(
            summary = "Get Current User Detail",
            description = "this endpoint is used to get user detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/current", "/current/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<UserResponse> get(@Parameter(hidden = true) User user) {
        log.info("controller : get user detail request by user-id : " + user.getId());
        UserResponse userResponse = userService.get(user);
        return Response.<UserResponse>builder().status("OK").message("user found").data(userResponse).build();
    }

    @Operation(
            summary = "Update User Detail",
            description = "this endpoint is used to update user detail and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PatchMapping(
            path = {"/current", "/current/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<UserResponse> update(@Parameter(hidden = true) User user, @RequestBody UpdateUserRequest request) {
        log.info("controller : update user request by user-id : " + user.getId());
        UserResponse updateResponse = userService.update(user, request);
        return Response.<UserResponse>builder().status("OK").message("user updated successfully").data(updateResponse).build();
    }

    @Operation(
            summary = "Delete User Account",
            description = "this endpoint is used to delete user's account and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @DeleteMapping(
            path = {"/current", "/current/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(@Parameter(hidden = true) User user){
        log.info("controller : delete user account by user-id : " + user.getId());
        userService.delete(user);
        return Response.builder().status("OK").message("user deleted successfully").build();
    }
}
