package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.product.CreateProductRequest;
import com.pmo.springcore.dto.product.ProductResponse;
import com.pmo.springcore.dto.product.UpdateProductRequest;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.service.IProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/products")
@Slf4j
public class ProductController {

    private final IProductService productService;

    @Autowired
    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @Operation(
            summary = "Create New Product",
            description = "this endpoint is used to create new product and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PostMapping(
            path = {"", "/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> create(@Parameter(hidden = true) User user, @RequestBody CreateProductRequest request) {
        log.info("controller : create new product by user-id : " + user.getId());
        ProductResponse productResponse = productService.create(user, request);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product created successfully")
                .data(productResponse)
                .build();
    }

    @Operation(
            summary = "Get Product By Product Id",
            description = "this endpoint is used to get product by product id",
            parameters = {
                    @Parameter(
                            name = "productId",
                            description = "Product Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @GetMapping(
            path = {"/{productId}", "/{productId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> getById(@PathVariable String productId) {
        log.info("controller : get product by id");
        ProductResponse productResponse = productService.getById(productId);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product found")
                .data(productResponse)
                .build();
    }

    @Operation(
            summary = "Get Product List",
            description = "this endpoint is used to get product list"
    )
    @GetMapping(
            path = {"/list", "/list/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Page<ProductResponse>> listProducts(Pageable pageable) {
        log.info("controller : get product list");
        Page<ProductResponse> productResponseList = productService.listProducts(pageable);
        return Response.<Page<ProductResponse>>builder()
                .status("OK")
                .message("here's products list")
                .data(productResponseList)
                .build();
    }

    @Operation(
            summary = "Update Product Detail",
            description = "this endpoint is used to update product and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "productId",
                            description = "Product Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @PatchMapping(
            path = {"/{productId}", "/{productId}/"},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<ProductResponse> update(@Parameter(hidden = true) User user, @RequestBody UpdateProductRequest request, @PathVariable String productId) {
        log.info("controller : update product by user-id : " + user.getId());
        ProductResponse productResponse = productService.update(user, request, productId);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product updated successfully")
                .data(productResponse)
                .build();
    }

    @Operation(
            summary = "Delete Product",
            description = "this endpoint is used to delete product and requires a custom header",
            parameters = {
                    @Parameter(
                            name = "X-API-TOKEN",
                            description = "Custom header for the request",
                            required = true,
                            in = ParameterIn.HEADER,
                            schema = @Schema(type = "string")
                    ),
                    @Parameter(
                            name = "productId",
                            description = "Product Id",
                            required = true,
                            in = ParameterIn.PATH,
                            schema = @Schema(type = "string")
                    )
            }
    )
    @DeleteMapping(
            path = {"/{productId}", "/{productId}/"},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<Object> delete(@Parameter(hidden = true) User user, @PathVariable String productId) {
        log.info("controller : delete product by user-id : " + user.getId());
        productService.delete(user, productId);
        return Response.builder()
                .status("OK")
                .message("product deleted successfully")
                .build();
    }
}
