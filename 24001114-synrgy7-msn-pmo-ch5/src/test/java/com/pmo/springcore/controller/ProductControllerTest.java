package com.pmo.springcore.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.product.CreateProductRequest;
import com.pmo.springcore.dto.product.ProductResponse;
import com.pmo.springcore.dto.product.UpdateProductRequest;
import com.pmo.springcore.entity.Merchant;
import com.pmo.springcore.entity.Product;
import com.pmo.springcore.entity.User;
import com.pmo.springcore.repository.IMerchantRepository;
import com.pmo.springcore.repository.IOrderDetailRepository;
import com.pmo.springcore.repository.IProductRepository;
import com.pmo.springcore.repository.IUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    private final MockMvc mockMvc;

    private final IProductRepository productRepository;

    private final IMerchantRepository merchantRepository;

    private final IOrderDetailRepository orderDetailRepository;

    private final IUserRepository userRepository;

    private final ObjectMapper objectMapper;

    private UUID merchantId;

    @Autowired
    public ProductControllerTest(
            MockMvc mockMvc,
            IProductRepository productRepository,
            IMerchantRepository merchantRepository,
            IOrderDetailRepository orderDetailRepository,
            IUserRepository userRepository,
            ObjectMapper objectMapper
    ) {
        this.mockMvc = mockMvc;
        this.productRepository = productRepository;
        this.merchantRepository = merchantRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.userRepository = userRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setUp() {
        orderDetailRepository.deleteAll();
        productRepository.deleteAll();
        merchantRepository.deleteAll();
        userRepository.deleteAll();

        User user = new User();
        user.setToken("token");
        user.generateExpiredToken();
        user.setRoleId(2);
        userRepository.save(user);

        Merchant merchant = new Merchant();
        merchant.setMerchantName("test merchant");
        merchant.setMerchantLocation("test location");
        merchant.setOpen(true);
        merchant.setUser(user);
        merchantRepository.save(merchant);
        merchantId = merchant.getId();
    }

    @AfterEach
    void tearDown() {
        orderDetailRepository.deleteAll();
        productRepository.deleteAll();
        merchantRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void testCreateProductUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        CreateProductRequest req = new CreateProductRequest();
        req.setProductName("test product");
        req.setPrice(100_000L);
        req.setMerchantId(String.valueOf(merchant.getId()));

        mockMvc.perform(
                post("/api/v1/products/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateProductByUserRole() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        user.setRoleId(1);
        userRepository.save(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        CreateProductRequest req = new CreateProductRequest();
        req.setProductName("test product");
        req.setPrice(100_000L);
        req.setMerchantId(String.valueOf(merchant.getId()));

        mockMvc.perform(
                post("/api/v1/products/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateProductByDifferentOwner() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        User userNew = new User();
        userNew.setToken("beda-token");
        userNew.generateExpiredToken();
        userNew.setRoleId(2);
        userRepository.save(userNew);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        CreateProductRequest req = new CreateProductRequest();
        req.setProductName("test product");
        req.setPrice(100_000L);
        req.setMerchantId(String.valueOf(merchant.getId()));

        mockMvc.perform(
                post("/api/v1/products/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateProductWithNegativePrice() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        CreateProductRequest req = new CreateProductRequest();
        req.setProductName("test product");
        req.setPrice(-100_000L);
        req.setMerchantId(String.valueOf(merchant.getId()));

        mockMvc.perform(
                post("/api/v1/products/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testCreateProductSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        CreateProductRequest req = new CreateProductRequest();
        req.setProductName("test product");
        req.setPrice(100_000L);
        req.setMerchantId(String.valueOf(merchant.getId()));

        mockMvc.perform(
                post("/api/v1/products/")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            assertEquals(req.getProductName(), res.getData().getProductName());
            assertEquals(req.getPrice(), res.getData().getPrice());
            assertEquals(req.getMerchantId(), String.valueOf(res.getData().getMerchantId()));
        });
    }

    @Test
    void testGetProductByIdWithInvalidProductId() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                get("/api/v1/products/salah-" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testGetProductByIdSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                get("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            assertEquals(product.getProductName(), res.getData().getProductName());
            assertEquals(product.getPrice(), res.getData().getPrice());
            assertEquals(product.getId(), res.getData().getId());
            assertEquals(product.getMerchant().getId(), res.getData().getMerchantId());
        });
    }

    @Test
    void testUpdateProductUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(100_000L);

        mockMvc.perform(
                patch("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateProductByUserRole() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        user.setRoleId(1);
        userRepository.save(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(100_000L);

        mockMvc.perform(
                patch("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateProductByDifferentOwner() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        User userNew = new User();
        userNew.setToken("beda-token");
        userNew.generateExpiredToken();
        userNew.setRoleId(2);
        userRepository.save(userNew);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(100_000L);

        mockMvc.perform(
                patch("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateProductWithNegativePrice() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(-100_000L);

        mockMvc.perform(
                patch("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateProductInvalidProductId() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(100_000L);

        mockMvc.perform(
                patch("/api/v1/products/salah-" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testUpdateProductSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        UpdateProductRequest req = new UpdateProductRequest();
        req.setProductName("test product 2");
        req.setPrice(100_000L);

        mockMvc.perform(
                patch("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
                        .content(objectMapper.writeValueAsString(req))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertEquals("OK", res.getStatus());
            assertNotNull(res.getData());

            assertEquals(req.getProductName(), res.getData().getProductName());
            assertEquals(req.getPrice(), res.getData().getPrice());
        });
    }

    @Test
    void testDeleteProductUnauthorized() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                delete("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isUnauthorized()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteProductByUserRole() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        user.setRoleId(1);
        userRepository.save(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                delete("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isForbidden()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteProductByDifferentOwner() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        User userNew = new User();
        userNew.setToken("beda-token");
        userNew.generateExpiredToken();
        userNew.setRoleId(2);
        userRepository.save(userNew);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                delete("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "beda-token")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteProductInvalidProductId() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                delete("/api/v1/products/salah-" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNotNull(res.getErrors());
            assertEquals("ERROR", res.getStatus());
            assertNull(res.getData());
        });
    }

    @Test
    void testDeleteProductSuccess() throws Exception {
        User user = userRepository.findFirstByToken("token").orElse(null);
        assertNotNull(user);

        Merchant merchant = merchantRepository.findByIdAndUserId(merchantId, user.getId()).orElse(null);
        assertNotNull(merchant);

        Product product = new Product();
        product.setProductName("test product");
        product.setPrice(10_000L);
        product.setMerchant(merchant);
        productRepository.save(product);

        mockMvc.perform(
                delete("/api/v1/products/" + product.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "token")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            Response<ProductResponse> res = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(res.getErrors());
            assertNull(res.getData());
            assertEquals("OK", res.getStatus());

            Product product1 = productRepository.findById(product.getId()).orElse(null);
            assertNull(product1);
        });
    }
}