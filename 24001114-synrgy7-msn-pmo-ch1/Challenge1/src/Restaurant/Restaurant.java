package Restaurant;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Restaurant {
    private final ArrayList<ArrayList<Object>> menu = new ArrayList<>();
    private final HashMap<Integer, Integer> orderedMenu = new HashMap<>(); // <menuId, qty>

    public Restaurant() {
        String[] menuName = {"Nasi Goreng", "Mie Goreng", "Nasi + Ayam", "Es Teh Manis", "Es Jeruk"};
        int[] menuPrice = {15000, 13000, 18000, 3000, 5000};
        for (int i = 0; i < menuName.length; i++) {
            ArrayList<Object> menuDetail = new ArrayList<>();
            menuDetail.add(menuName[i]);
            menuDetail.add(menuPrice[i]);
            menu.add(menuDetail);
        }
    }

    public void printMenu() {
        System.out.println("==========================");
        System.out.println("Selamat datang di BinarFud");
        System.out.println("==========================\n");
        System.out.println("Silahkan pilih makanan :");
        for (int i = 0; i < this.menu.size(); i++) {
            StringBuilder menuDesc = new StringBuilder((i + 1) + ". ");
            String menuName = (String) this.menu.get(i).get(0);
            int menuPrice = (int) this.menu.get(i).get(1);
            menuDesc.append(menuName).append((menuName.length() > 8) ? "\t" : "\t\t").append("|\t").append(menuPrice);
            System.out.println(menuDesc);
        }
        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar Aplikasi");
    }

    public void orderMenu(int menuId, Scanner input) {
        System.out.println("=============================");
        System.out.println("Berapa pesanan anda");
        System.out.println("=============================\n");

        System.out.println(menu.get(menuId).get(0) + "\t\t" + menu.get(menuId).get(1));
        System.out.println("(input 0 untuk kembali)\n");
        System.out.print("qty => ");
        int qty = input.nextInt();

        // checker
        if (qty == 0) return;

        // checker if ordered menu exists
        if (orderedMenu.get(menuId) != null) {
            int curQty = orderedMenu.get(menuId);
            qty += curQty;
        }

        orderedMenu.put(menuId, qty);
    }

    public void printDetailMenu() {
        System.out.println("=============================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("=============================\n");

        for (int menuId : orderedMenu.keySet()) {
            System.out.println(generateDetailMenuRow(menuId));
        }

        System.out.println("---------------------------+");
        System.out.println(generateTotalPriceRow());

        System.out.println("\n1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke Menu Utama");
        System.out.println("0. Keluar Aplikasi");
        System.out.print("=> ");
    }

    public void printBill() {
        System.out.println("=============================");
        System.out.println("BinarFud");
        System.out.println("=============================\n");

        System.out.println("Terima kasih sudah memesan ");
        System.out.println("di BinarFud\n");

        System.out.println("Dibawah ini adalah pesanan anda");

        for (int menuId : orderedMenu.keySet()) {
            System.out.println(generateDetailMenuRow(menuId));
        }

        System.out.println("---------------------------+");
        System.out.println(generateTotalPriceRow());

        System.out.println("\nPembayaran : BinarCash\n");

        System.out.println("=============================");
        System.out.println("Simpan struk ini sebagai");
        System.out.println("bukti pembayaran");
        System.out.println("=============================");
    }

    public void printBillDoc() {
        try {
            String path = "./Struk/";
            String fileName = "struk.txt";

            File folder = new File(path);
            if (!folder.exists()) {
                if (!folder.mkdirs()) {
                    throw new IOException("Failed to create directory: " + path);
                }
            }

            File file = new File(path + fileName);

            if (!file.exists()) {
                file.createNewFile();
            }

            writeBill(file);
        } catch (Exception err) {
            System.out.println(err.getMessage());
        }
    }

    private void writeBill(File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        appendBillDetail(writer);
        writer.close();
    }

    private void appendBillDetail(FileWriter writer) throws IOException {
        writer.write("=============================\n");
        writer.write("BinarFud\n");
        writer.write("=============================\n\n");

        writer.write("Terima kasih sudah memesan\n");
        writer.write("di BinarFud\n\n");

        writer.write("Di bawah ini adalah pesanan anda\n\n");

        for (int menuId : orderedMenu.keySet()) {
            writer.write(generateDetailMenuRow(menuId).toString());
            writer.write("\n");
        }
        writer.write("---------------------------+\n");
        writer.write(generateTotalPriceRow().toString());
        writer.write("\n");

        writer.write("\nPembayaran : BinarCash\n\n");

        writer.write("=============================\n");
        writer.write("Simpan struk ini sebagai\n");
        writer.write("bukti pembayaran\n");
        writer.write("=============================\n");
    }

    private StringBuilder generateDetailMenuRow(int menuId) {
        String menuName = menu.get(menuId).get(0).toString();
        int menuPrice = (Integer) menu.get(menuId).get(1);
        int qty = orderedMenu.get(menuId);
        return new StringBuilder(menuName).append((menuName.length() > 11) ? "\t" : "\t\t").append(qty).append("\t").append(menuPrice * qty);
    }

    private StringBuilder generateTotalPriceRow() {
        return new StringBuilder().append("Total\t\t\t").append(getTotalQty()).append("\t").append(getTotalPrice());
    }

    private int getTotalPrice() {
        int totalPrice = 0;
        for (int menuId : orderedMenu.keySet()) {
            int menuPrice = (Integer) menu.get(menuId).get(1);
            int qty = orderedMenu.get(menuId);
            totalPrice += qty * menuPrice;
        }
        return totalPrice;
    }

    private int getTotalQty() {
        int totalQty = 0;
        for (int menuId : orderedMenu.keySet()) {
            totalQty += orderedMenu.get(menuId);
        }
        return totalQty;
    }
}
