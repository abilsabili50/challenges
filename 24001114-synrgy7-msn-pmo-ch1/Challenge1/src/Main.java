import Restaurant.Restaurant;

import java.util.Objects;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Restaurant resto = new Restaurant();
        Scanner input = new Scanner(System.in);
        try {
            do{
                resto.printMenu();
                System.out.print("=> ");
                int cmd = input.nextInt();
                int menuId = cmd - 1;

                switch (cmd){
                    case 1:
                        resto.orderMenu(menuId, input);
                        continue;
                    case 2:
                        resto.orderMenu(menuId, input);
                        continue;
                    case 3:
                        resto.orderMenu(menuId, input);
                        continue;
                    case 4:
                        resto.orderMenu(menuId, input);
                        continue;
                    case 5:
                        resto.orderMenu(menuId, input);
                        continue;
                    case 99:
                        resto.printDetailMenu();
                        int confirmation = input.nextInt();
                        switch(confirmation){
                            case 1:
                                resto.printBillDoc();
                                resto.printBill();
                                break;
                            case 2:
                                System.out.println("Kembali ke Menu Utama");
                                continue;
                            case 0:
                                break;
                            default:
                                System.out.println("Menu tidak ada");
                                System.out.print("Apakah anda ingin memesan kembali? (y/n) => ");
                                String checker = input.next();
                                if(Objects.equals(checker, "y")) continue;
                                break;
                        }
                    case 0:
                        break;
                    default:
                        System.out.println("Menu tidak ada");
                        System.out.print("Apakah anda ingin memesan kembali? (y/n) => ");
                        String checker = input.next();
                        if(Objects.equals(checker, "y")) continue;
                        break;
                }
                break;
            }while(true);
        }catch (Exception e){
            System.out.println("Client Error: invalid command");
        }
    }
}