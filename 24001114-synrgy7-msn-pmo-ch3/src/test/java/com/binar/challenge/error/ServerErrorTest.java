package com.binar.challenge.error;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServerErrorTest {
    static ServerError serverError;

    @Test
    void testGetResponse(){
        serverError = new ServerError("Error yang dibuat-buat");

        assertEquals("Server Error : Error yang dibuat-buat", serverError.getResponse());
    }

    @Test
    void testGetStatusCode(){
        serverError = new ServerError("");

        assertEquals(500, serverError.getStatusCode());
    }
}