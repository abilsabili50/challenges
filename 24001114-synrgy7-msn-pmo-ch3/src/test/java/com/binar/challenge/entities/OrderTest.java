package com.binar.challenge.entities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    static Order order = new Order();

    @BeforeAll
    static void setup(){
        order.addOrder(1, 12);
        order.addOrder(2, 24);
        order.addOrder(3, 36);
        order.addOrder(4, 48);
    }

    @Test
    void testGetQtyByMenuIdPositive(){
        assertEquals(12, order.getQtyByMenuId(1), "Quantity didn't match!");
        assertEquals(24, order.getQtyByMenuId(2), "Quantity didn't match!");
        assertEquals(36, order.getQtyByMenuId(3), "Quantity didn't match!");
        assertEquals(48, order.getQtyByMenuId(4), "Quantity didn't match!");
    }

    @Test
    void testGetQtyByMenuIdNegative(){
        assertEquals(0, order.getQtyByMenuId(5), "Quantity didn't match!");
    }

}