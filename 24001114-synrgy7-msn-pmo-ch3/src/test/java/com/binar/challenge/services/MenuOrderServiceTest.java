package com.binar.challenge.services;

import com.binar.challenge.entities.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class MenuOrderServiceTest {
    static ArrayList<Menu> menus = new ArrayList<>();
    static Order order;
    static MenuOrderService service;
    static int totalPrice;
    static int totalQty;

    @BeforeAll
    static void init(){
        menus.add(new Menu(1, "Nasi Goreng", 15000));
        menus.add(new Menu(2, "Nasi Bebek", 20000));
        menus.add(new Menu(3, "Nasi Bakar", 25000));
        menus.add(new Menu(4, "Nasi Telor", 10000));
    }

    @BeforeEach
    void resetOrderedMenu(){
        order = new Order();
        service = new MenuOrderService(menus, order);

        List<Integer> qty = Arrays.asList(
                (int) (Math.random() * 10 + 1),
                (int) (Math.random() * 10 + 1),
                (int) (Math.random() * 10 + 1),
                (int) (Math.random() * 10 + 1)
        );

        List<Integer> price = menus.stream().map(Menu::getPrice).collect(Collectors.toList());

        service.addOrder(1, qty.get(0));
        service.addOrder(2, qty.get(1));
        service.addOrder(3, qty.get(2));
        service.addOrder(4, qty.get(3));

        totalPrice = 0;
        totalQty = 0;

        for (int i = 0; i < qty.size(); i++) {
            totalPrice += qty.get(i) * price.get(i);
        }

        totalQty = qty.stream().reduce(0, Integer::sum);
    }

    @Test
    void testGetTotalPrice1(){
        assertEquals(totalPrice, service.getTotalPrice(), "Total price didn't match");
    }

    @Test
    void testGetTotalPrice2(){
        assertEquals(totalPrice, service.getTotalPrice(), "Total price didn't match");
    }

    @Test
    void testGetTotalPrice3(){
        assertEquals(totalPrice, service.getTotalPrice(), "Total price didn't match");
    }

    @Test
    void testGetTotalQty1(){
        assertEquals(totalQty, service.getTotalQty(), "Total quantity didn't match");
    }

    @Test
    void testGetTotalQty2(){
        assertEquals(totalQty, service.getTotalQty(), "Total quantity didn't match");
    }

    @Test
    void testGetTotalQty3(){
        assertEquals(totalQty, service.getTotalQty(), "Total quantity didn't match");
    }

    @Test
    void testGetDetailRow(){
        int iterator = 1;
        StringBuilder sb = new StringBuilder();
        for (Menu menu : menus) {
            int menuId = menu.getMenuId();
            int price = menu.getPrice();
            String menuName = menu.getName();
            int qty = order.getQtyByMenuId(menuId);
            if (qty == 0) {
                continue;
            }

            String row = "\n" + iterator + ".\t" +
                    menuName + "\t|\t" +
                    qty + "\t|\t" + price;

            sb.append(row);

            iterator++;
        }

        assertEquals(sb.toString(), service.getDetailRow(), "Detail row didn't match!");
    }

}