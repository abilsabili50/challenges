package com.binar.challenge.error;

import lombok.Data;

@Data
public class ErrResponse extends Exception {
    private final int code;
    public ErrResponse(String message, int code){
        super(message);
        this.code = code;
    }

}
