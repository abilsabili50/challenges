package com.binar.challenge.error;

public interface IErrResponse{
    public String getResponse();
    public int getStatusCode();
}
