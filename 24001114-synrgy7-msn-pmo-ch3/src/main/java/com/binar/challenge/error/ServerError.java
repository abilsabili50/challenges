package com.binar.challenge.error;

public class ServerError extends ErrResponse implements IErrResponse{
    public ServerError(String message){
        super(message, 500);
    }

    @Override
    public String getResponse(){
        return "Server Error : " + this.getMessage();
    }

    @Override
    public int getStatusCode() {return this.getCode();}
}
