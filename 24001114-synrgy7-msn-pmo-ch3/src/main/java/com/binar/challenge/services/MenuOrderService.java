package com.binar.challenge.services;

import com.binar.challenge.entities.*;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class MenuOrderService {
    private List<Menu> menus;
    private Order orders;

    public MenuOrderService(List<Menu> menus, Order orders) {
        this.menus = menus;
        this.orders = orders;
    }

    public void addOrder(int menuId, int qty) {
        qty += this.getQtyByMenuId(menuId);
        this.orders.addOrder(menuId, qty);
    }

    private int getQtyByMenuId(int menuId) {
        return this.orders.getQtyByMenuId(menuId);
    }

    public int getTotalPrice() {
        int totalPrice = 0;

        for (Menu menu : this.menus) {
            int menuId = menu.getMenuId();
            int price = menu.getPrice();
            int qty = this.getQtyByMenuId(menuId);
            totalPrice += qty * price;
        }

        return totalPrice;
    }

    public int getTotalQty() {
        HashMap<Integer, Integer> orderDetail = this.orders.getOrderDetail();
        int totalQty = 0;

        for (Map.Entry<Integer, Integer> menuId : orderDetail.entrySet()) {
            totalQty += menuId.getValue();
        }

        return totalQty;
    }

    public String getDetailRow() {
        int iterator = 1;
        StringBuilder sb = new StringBuilder();
        for (Menu menu : this.menus) {
            int menuId = menu.getMenuId();
            int price = menu.getPrice();
            String menuName = menu.getName();
            int qty = this.orders.getQtyByMenuId(menuId);
            if (qty == 0) {
                continue;
            }

            String row = "\n" + iterator + ".\t" +
                    menuName + "\t|\t" +
                    qty + "\t|\t" + price;

            sb.append(row);

            iterator++;
        }

        return sb.toString();
    }
}