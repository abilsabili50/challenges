package com.binar.challenge;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.binar.challenge.entities.*;
import com.binar.challenge.error.ServerError;
import com.binar.challenge.services.MenuOrderService;


public class App {
    private static final String DIVIDER = "====================================";
    private static final ArrayList<Menu> menus = new ArrayList<>();
    private static final Order order = new Order();
    private static final MenuOrderService service = new MenuOrderService(menus, order);

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        initializeMenu();

        boolean checker;
        do {
            int choice;
            if ((choice = printMenu(input)) > 0 && choice <= menus.size()) {
                addOrder(choice, input);
                checker = confirmation(input);
            } else if (choice == 99) {
                int command = payment(input);
                switch (command) {
                    case 1:
                        printReceipt();
                        checker = false;
                        break;
                    case 2:
                        checker = true;
                        break;
                    case 0:
                        checker = false;
                        break;
                    default:
                        System.out.println("Invalid Menu");
                        checker = confirmation(input);
                        break;
                }
            } else if (choice == 0) {
                checker = false;
            } else {
                System.out.println("Invalid Menu");
                checker = confirmation(input);
            }

        } while (checker);

        System.out.println("\nTerima kasih telah melakukan pesanan");

    }

    public static void initializeMenu() {
        menus.add(new Menu(1, "Nasi Goreng", 15000));
        menus.add(new Menu(2, "Nasi Bebek", 20000));
        menus.add(new Menu(3, "Nasi Bakar", 25000));
        menus.add(new Menu(4, "Nasi Telor", 10000));
    }

    public static int printMenu(Scanner input) {
        System.out.println(DIVIDER);
        System.out.println("Selamat datang di BinarFud");
        System.out.println(DIVIDER);
        System.out.println("Silahkan pilih makanan :");
        for (Menu curMenu : menus) {
            int menuId = curMenu.getMenuId();
            String menuName = curMenu.getName();
            int menuPrice = curMenu.getPrice();
            String menuDesc = menuId + ". " + menuName +
                    "\t|\t" +
                    menuPrice;
            System.out.println(menuDesc);
        }
        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar Aplikasi");
        System.out.print("=> ");
        return input.nextInt();
    }

    public static void addOrder(int menuId, Scanner input) {
        System.out.println(DIVIDER);
        System.out.println("Berapa pesanan anda");
        System.out.println(DIVIDER + "\n");

        menus.stream().filter(menu -> menu.getMenuId() == menuId)
                .findFirst().ifPresent(choosenMenu -> {
                    System.out.println(choosenMenu.getName() + "\t\t" + choosenMenu.getPrice());
                    System.out.println("(input 0 untuk kembali)\n");
                    System.out.print("qty => ");
                    int qty = input.nextInt();

                    // checker
                    if (qty == 0) return;

                    service.addOrder(choosenMenu.getMenuId(), qty);
                });


    }

    public static int payment(Scanner input) {
        printDetailOrder();
        return input.nextInt();
    }

    private static void printDetailOrder() {
        System.out.println(DIVIDER);
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println(DIVIDER);

        int totalPrice = service.getTotalPrice();
        int totalQty = service.getTotalQty();

        System.out.println(service.getDetailRow());

        System.out.println("---------------------------------------------------------+");
        System.out.println("Total\t\t\t\t" + totalQty + "\t\t" + totalPrice);

        System.out.println("\n1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke Menu Utama");
        System.out.println("0. Keluar Aplikasi");
        System.out.print("=> ");
    }

    public static void printReceipt() {
        try {
            File file = createReceiptFile();
            String detailOrder = generateDetailOrder();
            System.out.println(detailOrder);
            writeFile(file, detailOrder);
        } catch(ServerError err) {
            System.out.println(err.getResponse());
        }catch (IOException err) {
            System.out.println(err.getMessage());
        }
    }

    private static File createReceiptFile() throws ServerError, IOException {
        String path = "./Bill/";
        String fileName = "bill.txt";

        File folder = new File(path);
        if (!folder.exists() && !folder.mkdir()) {
            throw new ServerError("Failed to create directory : " + path);
        }

        File file = new File(path + fileName);

        if (!file.exists() && !file.createNewFile()) {
            throw new ServerError("Failed to create file : " + path + fileName);
        }

        return file;
    }

    private static void writeFile(File file, String text) {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(text);
        } catch (IOException err) {
            System.out.println(err.getMessage());
        }
    }

    private static String generateDetailOrder() {
        StringBuilder sb = new StringBuilder(DIVIDER + "\n");
        sb.append("BinarFud\n");
        sb.append(DIVIDER + "\n\n");
        sb.append("Terima kasih sudah memesan\n");
        sb.append("di BinarFud\n\n");
        sb.append("Dibawah ini adalah pesanan anda\n");
        sb.append(service.getDetailRow());
        sb.append("\n-----------------------------------+\n");

        sb.append("Total\t\t\t\t");
        sb.append(service.getTotalQty());
        sb.append("\t\t");
        sb.append(service.getTotalPrice());

        sb.append("\n\nPembayaran : BinarCash\n\n");
        sb.append(DIVIDER + "\n");
        sb.append("Simpan struk ini sebagai\n");
        sb.append("bukti pembayaran\n");
        sb.append(DIVIDER);

        return sb.toString();
    }

    public static boolean confirmation(Scanner input) {
        System.out.print("\nApakah anda ingin melakukan proses lain (y/n)?");
        String checker = input.next();
        return checker.equalsIgnoreCase("y");
    }
}
