package com.pmo.springcore.service;

import com.pmo.springcore.dto.merchant.*;
import com.pmo.springcore.entity.oauth.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IMerchantService {
    MerchantResponse create(User user, CreateMerchantRequest req);

    MerchantResponse getById(String merchantId);

    MerchantResponse update(User user, UpdateMerchantRequest req);

    void delete(User user, String merchantId);

    Page<MerchantResponse> listOpenMerchant(Boolean isOpen, Pageable pageable);
}
