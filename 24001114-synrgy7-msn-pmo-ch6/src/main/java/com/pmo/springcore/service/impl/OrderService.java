package com.pmo.springcore.service.impl;

import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.entity.Order;
import com.pmo.springcore.entity.oauth.User;
import com.pmo.springcore.repository.IOrderRepository;
import com.pmo.springcore.service.IOrderService;
import com.pmo.springcore.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class OrderService implements IOrderService {

    @Autowired
    private IOrderRepository orderRepository;

    @Override
    @Transactional
    public OrderResponse create(User user, CreateOrderRequest request) {
        Order order = new Order();
        order.setDestinationAddress(request.getDestinationAddress());
        order.setCompleted(request.getCompleted());
        order.setUser(user);
        orderRepository.save(order);

        return OrderResponse.builder()
                .id(order.getId())
                .orderTime(order.getOrderTime())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .userId(order.getUser().getId())
                .build();
    }

    @Override
    public OrderResponse getById(User user, String orderId) {
        UUID orderUUID = Util.convertStringIntoUUID(orderId);

        Order order = orderRepository.findByIdAndUserId(orderUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order not found"));

        return OrderResponse.builder()
                .id(order.getId())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .orderTime(order.getOrderTime())
                .userId(order.getUser().getId())
                .build();
    }

    @Override
    public Page<OrderResponse> listOrder(User user, Boolean completed, Pageable pageable) {
        Page<Order> orderPage =
                (Objects.isNull(completed))
                        ? orderRepository.findAllByUserId(user.getId(), pageable)
                        : orderRepository.findAllByUserIdAndCompleted(user.getId(), completed, pageable);

        List<OrderResponse> orderList = orderPage.getContent().stream().map(order -> OrderResponse.builder()
                .id(order.getId())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .orderTime(order.getOrderTime())
                .userId(order.getUser().getId())
                .build()).toList();

        return new PageImpl<>(orderList, orderPage.getPageable(), orderPage.getTotalElements());
    }

    @Override
    @Transactional
    public OrderResponse update(User user, UpdateOrderRequest request) {
        UUID orderUUID = Util.convertStringIntoUUID(request.getId());

        Order order = orderRepository.findByIdAndUserId(orderUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order not found"));

        order.setCompleted(request.getCompleted());

        orderRepository.save(order);

        return OrderResponse.builder()
                .id(order.getId())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .orderTime(order.getOrderTime())
                .userId(order.getUser().getId())
                .build();
    }

    @Override
    @Transactional
    public void delete(User user, String orderId) {
        UUID orderUUID = Util.convertStringIntoUUID(orderId);

        Order order = orderRepository.findByIdAndUserId(orderUUID, user.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "order not found"));

        orderRepository.deleteById(order.getId());
    }
}
