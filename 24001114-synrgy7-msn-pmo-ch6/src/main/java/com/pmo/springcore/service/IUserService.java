package com.pmo.springcore.service;

import com.pmo.springcore.dto.user.UserResponse;
import com.pmo.springcore.entity.oauth.User;

import java.security.Principal;

public interface IUserService {
    UserResponse getDetailProfile(Principal principal);

    User getUserIdToken(Principal principal);

}
