package com.pmo.springcore.service;

import com.pmo.springcore.dto.auth.*;
import org.springframework.ui.Model;

import java.io.IOException;

public interface IAuthService {
    String register(RegisterRequest request, Boolean isEnabled);

    LoginResponse signinGoogle(GoogleAuthRequest request) throws IOException;

    LoginResponse login(LoginRequest request);

    void confirmOTP(String otp);

    String activateAccountThymleaf(Model model, String otp);

    void forgetPassword(ForgetPasswordRequest request);

    void changePassword(ChangePasswordRequest request);
}
