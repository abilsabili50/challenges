package com.pmo.springcore.service.impl;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.pmo.springcore.dto.auth.*;
import com.pmo.springcore.entity.oauth.Role;
import com.pmo.springcore.entity.oauth.User;
import com.pmo.springcore.repository.oauth.IRoleRepository;
import com.pmo.springcore.repository.oauth.IUserRepository;
import com.pmo.springcore.service.IAuthService;
import com.pmo.springcore.service.IUserService;
import com.pmo.springcore.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class AuthService implements IAuthService {

    @Value("${LOGINURL:}")
    private String LOGINURL;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public String register(RegisterRequest request, Boolean isEnabled) {
        if (userRepository.checkExistingEmail(request.getUsername()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "can't use an existing username");
        }

        String[] userRole = {"ROLE_USER"};
        String[] adminRole = {"ROLE_ADMIN"};
        String[] pickedRole = userRole;
        if (request.getRoleName() != null && request.getRoleName().equalsIgnoreCase("ADMIN")) pickedRole = adminRole;

        User user = new User();
        user.setUsername(request.getUsername().toLowerCase());
        user.setFullname(request.getFullname());

        user.setEnabled(isEnabled);

        String password = encoder.encode(request.getPassword().replaceAll("\\s+", ""));
        List<Role> roles = roleRepository.findByNameIn(pickedRole);

        user.setRoles(roles);
        user.setPassword(password);

        if (!isEnabled) {
            emailService.setupAndSendEmail(user, "REGISTER");
        }

        userRepository.save(user);

        return isEnabled ? "user created successfully." : "user created successfully, please check your email for activation.";

    }

    @Override
    public LoginResponse signinGoogle(GoogleAuthRequest request) {
        GoogleCredential credential = new GoogleCredential().setAccessToken(request.getAccessToken());
        log.error("create google credentials instance");
        Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName("Oauth2").build();
        log.error("create oauth2 instance");
        Userinfoplus profile;
        try {
            profile = oauth2.userinfo().get().execute();
            profile.toPrettyString();
            log.error("try to get user info profile");
        } catch (IOException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid access token");
        }

        User user = userRepository.findOneByUsername(profile.getEmail()).orElse(null);
        log.error("get user by email from user info profile");
        if (user == null) {
            log.error("user not found");
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setUsername(profile.getEmail());
            registerRequest.setPassword(profile.getId());
            registerRequest.setFullname(profile.getName());

            register(registerRequest, true);
            log.error("register new user and return login response");
            return LoginResponse.builder()
                    .message("account created successfully")
                    .build();
        }

        log.error("user found");
        if (!user.isEnabled()) {
            log.error("user is enabled");
            emailService.setupAndSendEmail(user, "REGISTER");
            log.error("send otp and returning login response");
            return LoginResponse.builder()
                    .message("your account is disable, please check your email to activate")
                    .build();
        }
        log.error("user is enabled");
        if (!profile.getVerifiedEmail()) {
            log.error("provided email isn't verified by google");
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "email not verified properly");
        }
        log.error("provided email is verified by google");
        String oldPassword = user.getPassword();
        log.error("get old password");
        if (!encoder.matches(profile.getId(), oldPassword)) {
            log.error("password doesn't match with profile id");
            user.setPassword(encoder.encode(profile.getId()));
            log.error("set password to profile id");
            userRepository.save(user);
            log.error("save password into database");
        }
        log.error("old password match with profile id");

        // login
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(profile.getEmail());
        loginRequest.setPassword(profile.getId());
        log.error("create login request");
        LoginResponse loginResponse = login(loginRequest);
        log.error("call login function to get login response");

        user.setPassword(oldPassword);
        log.error("set password into old password");
        userRepository.save(user);
        log.error("save old password again for user can login manually and returning login response");
        return loginResponse;
    }

    @Override
    public LoginResponse login(LoginRequest request) {
        User user = userRepository.findOneByUsername(request.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "bad credentials"));

        if (!user.isEnabled()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "account is disabled");

        if (!encoder.matches(request.getPassword(), user.getPassword()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "bad credentials");

        String url = LOGINURL + "?username=" + request.getEmail() +
                "&password=" + request.getPassword() +
                "&grant_type=password" +
                "&client_id=my-client-web" +
                "&client_secret=password";

        ResponseEntity<Map> response = restTemplateBuilder.build().exchange(url, HttpMethod.POST, null, new ParameterizedTypeReference<Map>() {
        });

        if (response.getStatusCode() != HttpStatus.OK) {
            throw new ResponseStatusException(response.getStatusCode(), (String) Objects.requireNonNull(response.getBody()).get("error_description"));
        }

        return LoginResponse.builder()
                .access_token((String) Objects.requireNonNull(response.getBody()).get("access_token"))
                .refresh_token((String) Objects.requireNonNull(response.getBody()).get("refresh_token"))
                .expires_in(Long.valueOf((Integer) Objects.requireNonNull(response.getBody()).get("expires_in")))
                .token_type((String) Objects.requireNonNull(response.getBody()).get("token_type"))
                .scope((String) Objects.requireNonNull(response.getBody()).get("scope"))
                .jti((String) Objects.requireNonNull(response.getBody()).get("jti"))
                .message(null)
                .build();
    }

    @Override
    @Transactional
    public void confirmOTP(String otp) {
        if (!Util.validateOTPFormat(otp)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "otp is invalid");

        User userByOTP = userRepository.findOneByOTP(otp).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "otp is invalid"));

        if (userByOTP.isEnabled())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "account has been activated");

        if (userByOTP.getOtpExpiredDate().before(new Date()))
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, "otp is invalid");

        userByOTP.setEnabled(true);
        userByOTP.setOtp(null);
        userByOTP.setOtpExpiredDate(null);
        userRepository.save(userByOTP);
    }

    @Override
    @Transactional
    public String activateAccountThymleaf(Model model, String otp) {
        User user = userRepository.findOneByOTP(otp).orElse(null);
        if (null == user) {
            System.out.println("user null: tidak ditemukan");
            model.addAttribute("erordesc", "otp is invalid");
            model.addAttribute("title", "");
            return "register";
        }
        if (user.isEnabled()) {
            model.addAttribute("erordesc", "account has been activated, you can login now");
            model.addAttribute("title", "");
            return "register";
        }
        if (user.getOtpExpiredDate().before(new Date())) {
            model.addAttribute("erordesc", "Your token is expired. Please Get token again.");
            model.addAttribute("title", "");
            return "register";
        }
        user.setEnabled(true);

        userRepository.save(user);
        model.addAttribute("title", "Congratulations, " + user.getUsername() + ", you have successfully registered.");
        model.addAttribute("erordesc", "");
        return "register";
    }

    @Override
    public void forgetPassword(ForgetPasswordRequest request) {
        User user = userRepository.checkExistingEmail(request.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "email not found"));

        if (!user.isEnabled())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "account inactive, please activate your account");

        emailService.setupAndSendEmail(user, "FORGET_PASSWORD");
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordRequest request) {
        if (!Util.validateOTPFormat(request.getOtp()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid otp format");

        User user = userRepository.findOneByOTP(request.getOtp()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "otp is invalid"));

        if (user.getOtpExpiredDate().before(new Date()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "otp has expired");

        if (!request.getNewPassword().equals(request.getNewConfirmPassword()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid password");

        if (!user.isEnabled())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "account inactive, please activate your account");

        if (encoder.matches(request.getNewPassword(), user.getPassword()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "password has been used before");

        user.setPassword(encoder.encode(request.getNewPassword()));
        user.setOtp(null);
        user.setOtpExpiredDate(null);
        userRepository.save(user);
    }
}
