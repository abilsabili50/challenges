package com.pmo.springcore.entity.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRow {
    private String merchantName;
    private String productName;
    private String destination;
    private String orderTime;
    private Long quantity;
    private Long totalPrice;
}
