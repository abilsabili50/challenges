package com.pmo.springcore.entity;

import javax.persistence.*;

import com.pmo.springcore.entity.oauth.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "order_time")
    private LocalDateTime orderTime;

    @PrePersist
    protected void onCreate(){
        orderTime = LocalDateTime.now();
    }

    @Column(name = "destination_address")
    private String destinationAddress;

    private Boolean completed;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails;
}
