package com.pmo.springcore.dto.product;

import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateProductRequest {
    private String productName;

    @Min(value = 0, message = "product price can't have minus price")
    private Long price;

    private String id;
}
