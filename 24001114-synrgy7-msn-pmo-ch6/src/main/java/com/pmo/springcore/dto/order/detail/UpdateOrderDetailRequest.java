package com.pmo.springcore.dto.order.detail;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateOrderDetailRequest {
    @NotNull(message = "quantity can't be empty")
    private Long quantity;

    private String id;
}
