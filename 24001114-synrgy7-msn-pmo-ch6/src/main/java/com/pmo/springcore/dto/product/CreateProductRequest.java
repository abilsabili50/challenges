package com.pmo.springcore.dto.product;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateProductRequest {

    @NotBlank(message = "product name can't be empty")
    private String productName;

    @NotNull(message = "product price can't be empty")
    @Min(value = 0, message = "product price can't have minus price")
    private Long price;

    @NotBlank(message = "merchant id can't be empty")
    private String merchantId;
}
