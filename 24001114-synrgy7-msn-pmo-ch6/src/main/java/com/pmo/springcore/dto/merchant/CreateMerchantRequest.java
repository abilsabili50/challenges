package com.pmo.springcore.dto.merchant;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateMerchantRequest {
    @NotBlank(message = "merchant name can't be empty")
    private String merchantName;

    @NotBlank(message = "merchant name can't be empty")
    private String merchantLocation;

    @NotNull(message = "merchant name can't be empty")
    private Boolean isOpen;
}
