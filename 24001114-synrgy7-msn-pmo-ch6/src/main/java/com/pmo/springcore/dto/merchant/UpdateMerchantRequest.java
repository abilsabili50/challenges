package com.pmo.springcore.dto.merchant;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateMerchantRequest {
    private String id;
    @NotNull(message = "isOpen field can't be empty")
    private Boolean isOpen;
}
