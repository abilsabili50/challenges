package com.pmo.springcore.dto.order;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateOrderRequest {
    private String id;

    @NotNull(message = "completed field can't be empty")
    private Boolean completed;
}
