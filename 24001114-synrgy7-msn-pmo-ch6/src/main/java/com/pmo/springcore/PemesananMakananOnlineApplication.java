package com.pmo.springcore;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
//@EnableWebMvc
@OpenAPIDefinition
public class PemesananMakananOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PemesananMakananOnlineApplication.class, args);
	}

}
