package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.product.CreateProductRequest;
import com.pmo.springcore.dto.product.ProductResponse;
import com.pmo.springcore.dto.product.UpdateProductRequest;
import com.pmo.springcore.service.IProductService;
import com.pmo.springcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(path = "/products")
public class ProductController {
    @Autowired
    private IProductService productService;

    @Autowired
    private IUserService userService;

    @PostMapping(path = {"/", ""})
    public Response<ProductResponse> create(Principal principal, @Valid @RequestBody CreateProductRequest request){
        ProductResponse productResponse = productService.create(userService.getUserIdToken(principal), request);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product created successfully")
                .data(productResponse)
                .build();
    }

    @GetMapping(path = {"/{productId}", "/{productId}/"})
    public Response<ProductResponse> getById(@PathVariable String productId){
        ProductResponse productResponse = productService.getById(productId);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product retrieved successfully")
                .data(productResponse)
                .build();
    }

    @GetMapping(path = {"/list", "/list/"})
    public Response<Page<ProductResponse>> listProducts(Pageable pageable){
        Page<ProductResponse> productResponses = productService.listProducts(pageable);
        return Response.<Page<ProductResponse>>builder()
                .status("OK")
                .message("products retrieved successfully")
                .data(productResponses)
                .build();
    }

    @PatchMapping(path = {"/{productId}", "/{productId}/"})
    public Response<ProductResponse> update(Principal principal, @PathVariable String productId, @Valid @RequestBody UpdateProductRequest request){
        request.setId(productId);
        ProductResponse productResponse = productService.update(userService.getUserIdToken(principal), request);
        return Response.<ProductResponse>builder()
                .status("OK")
                .message("product updated successfully")
                .data(productResponse)
                .build();
    }

    @DeleteMapping(path = {"/{productId}", "/{productId}/"})
    public Response<Object> delete(Principal principal, @PathVariable String productId){
        productService.delete(userService.getUserIdToken(principal), productId);
        return Response.builder()
                .status("OK")
                .message("product deleted successfully")
                .build();
    }
}
