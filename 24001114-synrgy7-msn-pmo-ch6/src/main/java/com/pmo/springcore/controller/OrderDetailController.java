package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.detail.CreateOrderDetailRequest;
import com.pmo.springcore.dto.order.detail.OrderDetailResponse;
import com.pmo.springcore.dto.order.detail.UpdateOrderDetailRequest;
import com.pmo.springcore.service.IOrderDetailService;
import com.pmo.springcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path = "/orders")
public class OrderDetailController {
    @Autowired
    private IOrderDetailService orderDetailService;

    @Autowired
    private IUserService userService;

    @PostMapping(path = {"/details", "/details/"})
    public Response<OrderDetailResponse> create(Principal principal, @Valid @RequestBody CreateOrderDetailRequest request){
        OrderDetailResponse orderDetailResponse = orderDetailService.create(userService.getUserIdToken(principal), request);
        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("order detail created successfully")
                .data(orderDetailResponse)
                .build();
    }

    @GetMapping(path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"})
    public Response<OrderDetailResponse> getById(Principal principal, @PathVariable String orderDetailId){
        OrderDetailResponse orderDetailResponse = orderDetailService.getById(userService.getUserIdToken(principal), orderDetailId);
        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("order detail retrieved successfully")
                .data(orderDetailResponse)
                .build();
    }

    @GetMapping(path = {"/{orderId}/details/list", "/{orderId}/details/list/"})
    public Response<List<OrderDetailResponse>> listOrderDetail(Principal principal, @PathVariable String orderId){
        List<OrderDetailResponse> orderDetailResponses = orderDetailService.listByOrderId(userService.getUserIdToken(principal), orderId);
        return Response.<List<OrderDetailResponse>>builder()
                .status("OK")
                .message("details of your order retrieved successfully")
                .data(orderDetailResponses)
                .build();
    }

    @PatchMapping(path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"})
    public Response<OrderDetailResponse> update(Principal principal, @PathVariable String orderDetailId, @Valid @RequestBody UpdateOrderDetailRequest request){
        request.setId(orderDetailId);
        OrderDetailResponse orderDetailResponse = orderDetailService.update(userService.getUserIdToken(principal), request);
        return Response.<OrderDetailResponse>builder()
                .status("OK")
                .message("order detail updated successfully")
                .data(orderDetailResponse)
                .build();
    }

    @DeleteMapping(path = {"/details/{orderDetailId}", "/details/{orderDetailId}/"})
    public Response<Object> delete(Principal principal, @PathVariable String orderDetailId){
        orderDetailService.delete(userService.getUserIdToken(principal), orderDetailId);
        return Response.builder()
                .status("OK")
                .message("order detail deleted successfully")
                .build();
    }
}
