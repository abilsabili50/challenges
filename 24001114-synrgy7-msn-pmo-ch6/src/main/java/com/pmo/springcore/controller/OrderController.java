package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.order.CreateOrderRequest;
import com.pmo.springcore.dto.order.OrderResponse;
import com.pmo.springcore.dto.order.UpdateOrderRequest;
import com.pmo.springcore.service.IOrderService;
import com.pmo.springcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Objects;

@RestController
@RequestMapping(path = "/orders")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @Autowired
    private IUserService userService;

    @PostMapping(path = {"/", ""})
    public Response<OrderResponse> create(Principal principal, @Valid @RequestBody CreateOrderRequest request){
        OrderResponse orderResponse = orderService.create(userService.getUserIdToken(principal), request);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order created successfully")
                .data(orderResponse)
                .build();
    }

    @GetMapping(path = {"/{orderId}", "/{orderId}/"})
    public Response<OrderResponse> getById(Principal principal, @PathVariable String orderId){
        OrderResponse orderResponse = orderService.getById(userService.getUserIdToken(principal), orderId);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order retrieved successfully")
                .data(orderResponse)
                .build();
    }

    @GetMapping(path = {"/list", "/list/"})
    public Response<Page<OrderResponse>> listOrder(Principal principal, @RequestParam(name = "completed", required = false) Boolean completed, Pageable pageable){
        Page<OrderResponse> orderResponses = orderService.listOrder(userService.getUserIdToken(principal), (Objects.nonNull(completed)) ? completed : null, pageable);
        return Response.<Page<OrderResponse>>builder()
                .status("OK")
                .message("orders retrieved successfully")
                .data(orderResponses)
                .build();
    }

    @PatchMapping(path = {"/{orderId}", "/{orderId}/"})
    public Response<OrderResponse> update(Principal principal, @PathVariable String orderId, @Valid @RequestBody UpdateOrderRequest request){
        request.setId(orderId);
        OrderResponse orderResponse = orderService.update(userService.getUserIdToken(principal), request);
        return Response.<OrderResponse>builder()
                .status("OK")
                .message("order updated successfully")
                .data(orderResponse)
                .build();
    }

    @DeleteMapping(path = {"/{orderId}", "/{orderId}/"})
    public Response<Object> delete(Principal principal, @PathVariable String orderId){
        orderService.delete(userService.getUserIdToken(principal), orderId);
        return Response.<Object>builder()
                .status("OK")
                .message("order deleted successfully")
                .build();
    }
}
