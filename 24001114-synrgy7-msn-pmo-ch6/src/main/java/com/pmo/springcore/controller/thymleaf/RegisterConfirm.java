package com.pmo.springcore.controller.thymleaf;

import com.pmo.springcore.entity.oauth.User;
import com.pmo.springcore.repository.oauth.IUserRepository;
import com.pmo.springcore.service.IAuthService;
import com.pmo.springcore.service.IUserService;
import com.pmo.springcore.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/user-register/web/")
public class RegisterConfirm {

    @Autowired
    public IAuthService authService;

    @GetMapping(value = { "/index/{otp}"})
    public String index(Model model,@PathVariable String  otp) {
        return authService.activateAccountThymleaf(model, otp);

    }
}

