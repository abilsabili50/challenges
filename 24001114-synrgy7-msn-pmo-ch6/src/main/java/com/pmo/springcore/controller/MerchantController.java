package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.merchant.CreateMerchantRequest;
import com.pmo.springcore.dto.merchant.MerchantResponse;
import com.pmo.springcore.dto.merchant.UpdateMerchantRequest;
import com.pmo.springcore.service.IMerchantService;
import com.pmo.springcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(path = "/merchants")
public class MerchantController {
    @Autowired
    private IMerchantService merchantService;

    @Autowired
    private IUserService userService;

    @PostMapping(path = {"/", ""})
    public Response<MerchantResponse> create(Principal principal, @Valid @RequestBody CreateMerchantRequest request){
        MerchantResponse merchantResponse = merchantService.create(userService.getUserIdToken(principal), request);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant created successfully")
                .data(merchantResponse)
                .build();
    }

    @GetMapping(path = {"/{merchantId}", "/{merchantId}/"})
    public Response<MerchantResponse> getById(@PathVariable String merchantId){
        MerchantResponse merchantResponse = merchantService.getById(merchantId);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant retrieved successfully")
                .data(merchantResponse)
                .build();
    }

    @GetMapping(path = {"/list", "/list/"})
    public Response<Page<MerchantResponse>> listMerchants(Pageable pageable, @RequestParam(name = "is_open", defaultValue = "true") Boolean isOpen){
        Page<MerchantResponse> merchantResponses = merchantService.listOpenMerchant(isOpen, pageable);
        return Response.<Page<MerchantResponse>>builder()
                .status("OK")
                .message("merchants retrieved successfully")
                .data(merchantResponses)
                .build();
    }

    @PatchMapping(path = {"/{merchantId}", "/{merchantId}/"})
    public Response<MerchantResponse> update(Principal principal, @PathVariable String merchantId, @Valid @RequestBody UpdateMerchantRequest request){
        request.setId(merchantId);
        MerchantResponse merchantResponse = merchantService.update(userService.getUserIdToken(principal), request);
        return Response.<MerchantResponse>builder()
                .status("OK")
                .message("merchant updated successfully")
                .data(merchantResponse)
                .build();
    }

    @DeleteMapping(path = {"/{merchantId}", "/{merchantId}/"})
    public Response<Object> delete(Principal principal, @PathVariable String merchantId){
        merchantService.delete(userService.getUserIdToken(principal), merchantId);
        return Response.builder()
                .status("OK")
                .message("merchant deleted successfully")
                .build();
    }
}
