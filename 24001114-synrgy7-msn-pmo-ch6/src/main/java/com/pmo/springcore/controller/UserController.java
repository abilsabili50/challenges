package com.pmo.springcore.controller;

import com.pmo.springcore.dto.Response;
import com.pmo.springcore.dto.user.UserResponse;
import com.pmo.springcore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping(path = {"/current", "/current/"})
    public Response<UserResponse> getDetailProfile(Principal principal){
        UserResponse userResponse = userService.getDetailProfile(principal);

        return Response.<UserResponse>builder()
                .status("OK")
                .message("detail user retrieved successfully")
                .data(userResponse)
                .build();
    }
}
