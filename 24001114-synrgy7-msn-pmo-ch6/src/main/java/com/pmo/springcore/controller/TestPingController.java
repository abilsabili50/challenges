package com.pmo.springcore.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestPingController {

    @GetMapping(path = "/ping")
    public String hello(){
        return "Hello";
    }
}
