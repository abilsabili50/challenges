package com.pmo.springcore.repository.oauth;


import com.pmo.springcore.entity.oauth.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientRepository extends JpaRepository<Client, Long> {

    Client findOneByClientId(String clientId);

}

