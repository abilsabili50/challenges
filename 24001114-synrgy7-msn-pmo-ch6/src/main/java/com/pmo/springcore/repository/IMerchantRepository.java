package com.pmo.springcore.repository;

import com.pmo.springcore.entity.Merchant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IMerchantRepository extends JpaRepository<Merchant, UUID> {
    Boolean existsByMerchantNameAndUserId(String merchantName, Long user_id);
    Boolean existsByIdAndUserId(UUID id, Long user_id);
    Optional<Merchant> findByIdAndUserId(UUID id, Long user_id);
    @Query("SELECT m FROM Merchant m WHERE m.isOpen = :isOpen")
    Page<Merchant> findAllByIsOpen(Boolean isOpen, Pageable pageable);
}
