package com.pmo.springcore.repository;

import com.pmo.springcore.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface IOrderRepository extends JpaRepository<Order, UUID> {
    Optional<Order> findByIdAndUserId(UUID id, Long user_id);

    @Query("SELECT o FROM Order o WHERE o.user.id = :userId")
    Page<Order> findAllByUserId(@Param("userId") Long userId, Pageable pageable);

    @Query("SELECT o FROM Order o WHERE o.user.id = :userId AND o.completed = :completed")
    Page<Order> findAllByUserIdAndCompleted(@Param("userId") Long userId, @Param("completed") Boolean completed, Pageable pageable);

    @Query("SELECT o FROM Order o where o.user.id = :userId")
    List<Order> findAllByUserId(@Param("userId") Long userId);
}
