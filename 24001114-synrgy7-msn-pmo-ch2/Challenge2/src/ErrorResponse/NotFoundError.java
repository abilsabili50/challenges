package ErrorResponse;

public class NotFoundError extends ErrResponse implements IErrResponse{

    public NotFoundError(String message) {
        super(message, 404);
    }

    @Override
    public String getResponse(){
        return "Not Found Error : " + this.getMessage();
    }
}
