package ErrorResponse;

public class ServerError extends ErrResponse implements IErrResponse{
    public ServerError(String message){
        super(message, 500);
    }

    @Override
    public String getResponse(){
        return "Server Error : " + this.getMessage();
    }
}
